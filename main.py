import random

import numpy as np


class HelloWorldGame:
    def __init__(self, h, w):
        self.w = w
        self.h = h
        self.state = HelloWorldState(h, w, h // 2, w // 2, HelloWorldState.Directions.NORTH, 0, 0)

    def act(self, action):
        reward = -1
        if action == HelloWorldState.Actions.LEFT:
            self.state.d = (self.state.d - 1) % 4
        elif action == HelloWorldState.Actions.RIGHT:
            self.state.d = (self.state.d + 1) % 4
        elif action == HelloWorldState.Actions.FORWARD:
            dx = 0
            dy = 0
            if self.state.d == HelloWorldState.Directions.NORTH:
                dy = -1
            elif self.state.d == HelloWorldState.Directions.EAST:
                dx = 1
            elif self.state.d == HelloWorldState.Directions.SOUTH:
                dy = 1
            elif self.state.d == HelloWorldState.Directions.WEST:
                dx = -1
            self.state.px = (self.state.px + dx) % self.w
            self.state.py = (self.state.py + dy) % self.h

            if (self.state.px, self.state.py) == (self.state.tx, self.state.ty):
                reward = 10
                while (self.state.px, self.state.py) == (self.state.tx, self.state.ty):
                    self.state.tx, self.state.ty = random.randrange(self.w), random.randrange(self.h)

        return reward, self.state

    def getState(self):
        return self.state


class HelloWorldState():
    class Actions:
        LEFT = 0
        RIGHT = 1
        FORWARD = 2

    class Directions:
        NORTH = 0
        EAST = 1
        SOUTH = 2
        WEST = 3

    def __init__(self, h, w, py, px, d, ty, tx):
        self.h = h
        self.w = w
        self.px = px
        self.py = py
        self.d = d
        self.tx = tx
        self.ty = ty

    def __str__(self):
        res = ""
        for y in range(self.h):
            for x in range(self.w):
                if (self.px, self.py) == (x, y):
                    res += {0: "^", 1: ">", 2: "v", 3: "<"}[self.d]
                elif (self.tx, self.ty) == (x, y):
                    res += "x"
                else:
                    res += "."
            res += "\n"
        res += "\n"
        return res


class ConsolePlayer():
    def getAction(self, gamestate):
        inp = input()
        return \
        {"w": HelloWorldState.Actions.FORWARD, "a": HelloWorldState.Actions.LEFT, "d": HelloWorldState.Actions.RIGHT}[
            inp]

    def setReward(self, reward, newState):
        pass


class QTablePlayer():
    def __init__(self, game):
        self.w = game.w
        self.h = game.h
        self.dNum = 4
        self.aNum = 3
        self.pyStride = self.w * self.dNum * self.h * self.w
        self.pxStride = self.dNum * self.h * self.w
        self.dStride = self.h * self.w
        self.tyStride = self.w
        self.qtable = np.zeros([self.calculateStateCount(), self.aNum])
        self.stateIndexCopy = None
        self.actionCopy = None
        self.lr = 1.0
        self.dfr = 1.0

    def calculateStateIndex(self, state):
        return state.py * self.pyStride + state.px * self.pxStride + state.d * self.dStride + state.ty * self.tyStride + state.tx

    def calculateStateCount(self):
        return self.w * self.h * self.dNum * self.w * self.h

    def getAction(self, gamestate):
        self.stateIndexCopy = self.calculateStateIndex(gamestate)
        self.actionCopy = np.argmax(self.qtable[self.stateIndexCopy])
        return self.actionCopy

    def setReward(self, reward, newState):
        newStateIndex = self.calculateStateIndex(newState)
        lastQval = self.qtable[self.stateIndexCopy, self.actionCopy]
        targetQval = reward + self.dfr * np.max(self.qtable[newStateIndex])
        newQval = lastQval + self.lr * (targetQval - lastQval)
        self.qtable[self.stateIndexCopy, self.actionCopy] = newQval


def main():
    game = HelloWorldGame(7, 7)
    player = QTablePlayer(game)

    state = game.getState()
    print(state)

    i = 0
    while (True):
        action = player.getAction(state)
        reward, state = game.act(action)
        player.setReward(reward, state)
        print(state)

        if i % 100000 < 15:
            input()

        i += 1


if __name__ == "__main__":
    main()
