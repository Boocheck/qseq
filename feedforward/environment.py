from feedforward.action import FeedforwardAction
from feedforward.state import FeedforwardEnvironmentState
from framework.action import Action
from framework.environment.environtment import Environment
from framework.environment.response import EnvironmentResponse
from framework.environment.reward import Reward
from sequence.action import SequenceAction, ActionType
from sequence.state import SequenceEnvironmentState
import numpy as np


class FeedforwardEnvironment(Environment):
    def __init__(self, state, stage_size):
        super().__init__(state)
        self.stage_size = stage_size

    def validate(self, action: Action) -> bool:
        pass

    def react(self, action: Action) -> EnvironmentResponse:

        if isinstance(action, FeedforwardAction) and isinstance(self._environment_state, FeedforwardEnvironmentState):

            state = FeedforwardEnvironmentState(
                np.clip(self._environment_state.player + action.data, -self.stage_size, self.stage_size),
                self._environment_state.power_up.copy()
            )

            dist_before = FeedforwardEnvironmentState.dist(self._environment_state.player, self._environment_state.power_up)
            dist_after = FeedforwardEnvironmentState.dist(state.player, state.power_up)
            reward = dist_before - dist_after - 0.1
            # reward = (1.0 if state.is_terminal() else 0.0) - 0.1
            self._environment_state = state
            return EnvironmentResponse(state, Reward(reward))
        else:
            raise ValueError()
