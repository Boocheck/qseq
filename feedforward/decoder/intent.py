import numpy as np

from framework.agent.intent import Intent


class FeedforwardIntent(Intent):
    def __init__(self, intent: np.ndarray):
        self.intent = intent
