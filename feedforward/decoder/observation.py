import numpy as np

from framework.agent.observation import Observation


class FeedforwardObservation(Observation):
    def __init__(self, player: np.ndarray, power_up: np.ndarray):
        self.player = player
        self.power_up = power_up

    def __repr__(self):
        return "<FeedforwardObservation player: {!r} power up: {!r}".format(self.player, self.power_up)
