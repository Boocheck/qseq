import random
from typing import List

import datetime
import numpy as np
import tensorflow as tf

from feedforward.action import FeedforwardAction
from feedforward.decoder.intent import FeedforwardIntent
from feedforward.decoder.observation import FeedforwardObservation
from feedforward.state import FeedforwardEnvironmentState
from framework.action import Action
from framework.agent.agent import Agent
from framework.agent.intent import Intent
from framework.agent.observation import Observation
from framework.agent.state import AgentState
from framework.algorithm.experience import Experience
from framework.algorithm.stop import StopCondition
from framework.environment.state import EnvironmentState
from framework.state import State
from sequence.action import SequenceAction, ActionType
from sequence.agent import SequenceAgent
from sequence.decoder.intent import DecoderIntent
from sequence.decoder.observation import DecoderObservation
from sequence.state import SequenceEnvironmentState
from utils.combining import destructuring, combine


class FeedforwardAgent(Agent):
    def __init__(self, target_model:"FeedforwardAgent" = None, agent_state= None):

        self._target_model = target_model
        self._stop_condition = None
        self._graph = tf.Graph()
        self._session = tf.Session(graph=self._graph)
        self._discount_factor = 0.99
        self._train_iter = 0

        with self._graph.as_default():

            self._input_player = tf.placeholder(tf.float32, [None, 2], name="input_player")
            self._input_power_up = tf.placeholder(tf.float32, [None, 2], name="input_power_up")
            self._q_target = tf.placeholder(tf.float32, [None, 1], name="q_target")
            self._sigma = tf.placeholder(tf.float32, [])

            self._reward = tf.placeholder(tf.float32, [None, 1], name="reward")
            # self._dist_change = tf.placeholder(tf.float32, [None, 1], name="dist_change")

            with tf.variable_scope("actor") as vs:
                self._actor_input = tf.concat((self._input_player, self._input_power_up), 1)
                self._actor_dense1 = tf.layers.dense(self._actor_input, 32, name="actor_dense1")
                # self._actor_norm1 = tf.layers.batch_normalization(self._actor_dense1, name="actor_norm1")
                self._actor_activation1 = tf.tanh(self._actor_dense1, name="actor_activation1")
                self._actor_dense2 = tf.layers.dense(self._actor_activation1, 2, name="actor_dense2", kernel_initializer=tf.initializers.random_uniform(-0.001, 0.001), bias_initializer=tf.initializers.random_uniform(-0.001, -0.001))
                # self._actor_norm2 = tf.layers.batch_normalization(self._actor_dense2, name="actor_norm2")
                self._actor_activation2 = tf.tanh(self._actor_dense2, name="actor.activation2")
                self._actor_output = self._actor_activation2
                # if(self._target_model is not None):
                #     self._actor_output = self._actor_activation2 + tf.random_normal(tf.shape(self._actor_activation2), 0.0, self._sigma)
                self._actor_variables = [v for v in tf.global_variables() if vs.name in v.name]

            with tf.variable_scope("critic") as vs:
                # self._critic_input = tf.concat([self._actor_output, self._actor_input], 1)
                self._critic_dense_input = tf.layers.dense(self._actor_input, 32, name="critic_dense_input")
                # self._critic_norm_input = tf.layers.batch_normalization(self._critic_dense_input, name="critic_norm_input")
                self._critic_activation_input = tf.tanh(self._critic_dense_input, name="critic_activation_input")
                self._critic_input_adapter = tf.layers.dense(self._critic_activation_input, 32, name="critic_input_adapter")
                self._critic_action_adapter = tf.layers.dense(self._actor_output, 32, name="critic_action_adapter")
                self._critic_merged = tf.tanh(self._critic_input_adapter+self._critic_action_adapter, name="critic_merged")
                self._critic_output = tf.layers.dense(self._critic_merged, 1, name="critic_output", kernel_initializer=tf.initializers.random_uniform(-0.001, 0.001), bias_initializer=tf.initializers.random_uniform(-0.001, -0.001))
                self._critic_variables = [v for v in tf.global_variables() if vs.name in v.name]

            self._grad_q_wrt_a=tf.gradients(-tf.reduce_mean(self._critic_output), self._actor_variables)

            actor_layers = [
                self._actor_input,
                self._actor_dense1,
                # self._actor_norm1,
                self._actor_activation1,
                self._actor_dense2,
                # self._actor_norm2,
                self._actor_activation2
            ]
            critic_layers = [
                self._critic_dense_input,
                # self._critic_norm_input,
                self._critic_activation_input,
                self._critic_input_adapter,
                self._critic_action_adapter,
                self._critic_merged,
                self._critic_output
            ]

            self._actor_minimize = tf.train.AdamOptimizer(0.001).minimize(tf.reduce_mean(-self._critic_output), var_list=self._actor_variables)
            # self._actor_minimize = tf.train.AdamOptimizer(0.001).apply_gradients(zip(self._grad_q_wrt_a, self._actor_variables))
            self._critic_minimize = tf.train.AdamOptimizer(0.001).minimize(
                tf.reduce_mean(tf.square(self._critic_output - self._q_target)), var_list=self._critic_variables)

            self.initializer = tf.global_variables_initializer()

            tf.summary.scalar("q", tf.reduce_mean(self._critic_output))
            tf.summary.scalar("q_error", tf.reduce_mean(self._critic_output-self._q_target))
            tf.summary.scalar("reward", tf.reduce_mean(self._reward))

            for variable in self._actor_variables + self._critic_variables:
                tf.summary.histogram("variable_"+variable.name, variable)

            for op in actor_layers+critic_layers:
                tf.summary.histogram("op_"+op.name, op)

            # tf.summary.scalar("dist_change", tf.reduce_mean(self._dist_change))

            self._summaries = tf.summary.merge_all()

        super().__init__(agent_state)
        self._session.run(self.initializer)
        if target_model is not None:
            target_model.sync_to(self)
            self._writer = tf.summary.FileWriter("logdir/" + datetime.datetime.now().isoformat("_").replace(":", "_"), self._graph)



    def act(self, intent: Intent) -> Action:
        if isinstance(intent, FeedforwardIntent):
            return FeedforwardAction(intent.intent)
        else:
            raise ValueError()

    def train(self, experience_batch: List[Experience]):
        dict_before = self._prepare_batch([x.get_before() for x in experience_batch])
        dict_after = self._prepare_batch([x.get_after() for x in experience_batch])
        dict_before_target = self._target_model._prepare_batch([x.get_before() for x in experience_batch])
        dict_after_target = self._target_model._prepare_batch([x.get_after() for x in experience_batch])
        rewards = np.array([e.get_reward().get_value() for e in experience_batch]).reshape([-1,1])
        terminal = np.array([(0. if self._stop(e.get_after().get_environment_state()) else 1.) for e in experience_batch]).reshape([-1, 1])
        actions = combine(experience_batch, lambda x: x.get_intent().intent)


        def calc_q(prefix = None):
            q_before = self._session.run(self._critic_output, dict_before)
            q_after = self._session.run(self._critic_output, dict_after)
            q_before_target = self._target_model._session.run(self._target_model._critic_output, dict_before_target)
            q_after_target = self._target_model._session.run(self._target_model._critic_output, dict_after_target)
            q_bellman =rewards + terminal * self._discount_factor * q_after_target

            if prefix is not None:
                template = "{}: pre-act {:7.4f}({:7.4f}), post-act {:7.4f}({:7.4f}), action {}, reward {:7.4f}, bellman {:7.4f}, error {:7.4f}".format(
                    prefix,
                    q_before[0, 0].item(),
                    q_before_target[0, 0].item(),
                    q_after[0, 0].item(),
                    q_after_target[0, 0].item(),
                    actions[0],
                    rewards[0,0].item(),
                    q_bellman[0, 0].item(),
                    q_bellman[0, 0].item()-q_before[0, 0].item()
                )
                print(template)
            return q_before, q_after, q_before_target, q_after_target, q_bellman


        q_before, q_after, q_before_target, q_after_target, q_bellman = calc_q("pre training")

        dict_before_with_q = dict_before.copy()
        dict_before_with_q.update({
            self._q_target: q_bellman,
            self._actor_output: actions
        })



        self._session.run(self._critic_minimize, dict_before_with_q)

        q_before, q_after, q_before_target, q_after_target, q_bellman = calc_q("after critic")

        self._session.run(self._actor_minimize, dict_before)
        # TODO[boocheck]: this training loop is experimental
        # self._session.run([self._actor_minimize, self._critic_minimize], dict_before_with_q)

        q_before, q_after, q_before_target, q_after_target, q_bellman = calc_q("after actor ")

        self._target_model.sync_to(self, False, 0.1)

        q_before, q_after, q_before_target, q_after_target, q_bellman = calc_q("after syncin")



        if self._train_iter % 1 ==0 :
            experience_batch[0].get_before().get_environment_state()
            summ = self._session.run(self._summaries, {self._reward: rewards, self._q_target: q_bellman, **dict_before})
            self._writer.add_summary(summ, self._train_iter)

        self._train_iter += 1


    def observe(self, environment_state: EnvironmentState) -> Observation:
        s = environment_state
        if not isinstance(s, FeedforwardEnvironmentState):
            raise ValueError()

        return FeedforwardObservation(s.player, s.power_up)

    def try_intend(self, observation: Observation, explore=0.0) -> (Intent, AgentState):
        if not isinstance(observation, FeedforwardObservation):
            raise ValueError()

        if random.random() < explore:
            actor_output = np.random.uniform(-1.0, 1.0, [1, 2])
            print("RANDOM")
        else:
            actor_output = self._session.run(self._actor_output,
                {
                    self._input_player: observation.player,
                    self._input_power_up: observation.power_up,
                    self._sigma: self.calc_sigma()
                }
            )

        return FeedforwardIntent(actor_output), self._agent_state

    def sync_to(self, model: Agent, with_state: bool = True, tau: float = 1.0) -> bool:
        if not isinstance(model, FeedforwardAgent):
            raise ValueError()


        if with_state:
            self.set_state(model.get_state())

        variables_number = 0
        variables_errors = 0
        with self._graph.as_default():
            for v in tf.global_variables():
                try:
                    variables_number += 1
                    current = self._session.run(v)
                    other = model._session.run(model._graph.get_tensor_by_name(v.name))
                    new_value = other * tau + (1 - tau) * current
                    v.load(new_value, self._session)
                except Exception as e:
                    variables_errors += 1

        return variables_errors == 0



    def _create_initial_state(self):
        return FeedforwardAgentState()

    def _prepare_batch(self, batch: List[State]):
        observations = [self.observe(x.get_environment_state()) for x in batch]

        batch_dict = destructuring({
            self._input_player: lambda: combine(observations, lambda x: x.player),
            self._input_power_up: lambda: combine(observations, lambda x: x.power_up),
            self._sigma: self.calc_sigma
        })

        return batch_dict

    def _stop(self, environment_state: EnvironmentState):
        return self._stop_condition.is_terminal(
            environment_state) if self._stop_condition is not None else environment_state.is_terminal()

    def calc_sigma(self):
        return 1.0*0.9**(self._train_iter/1000)

    def close(self):
        self._writer.close()






class FeedforwardAgentState(AgentState):
    def __repr__(self):
        return "<FeedforwardAgentState>"
