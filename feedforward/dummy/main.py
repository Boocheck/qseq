import random

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from feedforward.decoder.agent import FeedforwardAgent
from feedforward.decoder.intent import FeedforwardIntent
from feedforward.dummy.generator import FeedforwardGenerator
from feedforward.environment import FeedforwardEnvironment
from feedforward.state import FeedforwardEnvironmentState
from framework.algorithm.trainer import Trainer
from feedforward.action import FeedforwardAction
import matplotlib.animation as anim

# setings
size = 5
tick = 0.5

# drawing
x = np.arange(-size, size+tick, tick)
y = np.arange(-size, size+tick, tick)
qfield = np.zeros([len(x), len(y)])
qfield[0,0] = 1.0
u, v = np.meshgrid(x, y)
fig, ax = plt.subplots()
ax.set_xlim(-size,size)
ax.set_ylim(-size, size)
quiver = ax.quiver(x,y,u,v)
im = ax.imshow(qfield, origin="lower", extent=(-size-.5*tick, size+.5*tick, -size-.5*tick, size+.5*tick))
# ax.quiverkey(q, X=0.3, Y=1.1, U=10, label='Quiver key, length = 10', labelpos='E')
power_up = ax.scatter([5], [7], c="b")
player = ax.scatter([0], [0], c="r")
noise_step = 0

#
#
#
# file_sup = open("log_supervised.txt", "rt")
# file_unsup = open("log_arch_every.txt", "rt")
# unsup = [float(x) for x in file_unsup.readlines()]
# sup = [float(x) for x in file_sup.readlines()]
# plt.plot(range(len(unsup)), unsup, range(len(sup)), sup)
# plt.legend(["reinforced", "supervised"])
# plt.show()

def draw_callback(episode:int , step:int, agent:FeedforwardAgent, env:FeedforwardAgent):
    state = env.get_state()

    if step % 1 == 0:
        if isinstance(state, FeedforwardEnvironmentState):
            player.set_offsets(state.player)
            power_up.set_offsets(state.power_up)
            plt.pause(0.05)

            # input prep
            rows=[]
            for py in y:
                for px in x:
                    rows.append([px, py, state.power_up[0,0], state.power_up[0,1]])
            batch = np.array(rows)

            # querying agent
            actions, q, = agent._session.run([agent._actor_output, agent._critic_output], {agent._actor_input: batch, agent._sigma: agent.calc_sigma()})
            u, v, qm = actions[:, 0].reshape([-1, len(x)]), actions[:, 1].reshape([-1, len(x)]), q.reshape(-1, len(x))

            # updating
            quiver.set_UVC(u,v)
            qm = (qm-np.min(qm))/(np.max(qm)-np.min(qm))
            im.set_data(qm)


def noise_callback(intent):
    global noise_step
    if isinstance(intent, FeedforwardIntent):
        # sigma = 0.95**(noise_step/1000)
        sigma = (1-noise_step/100000)
        noise_step+=1
        return FeedforwardIntent(intent.intent+np.random.normal(0.0, sigma, [1, 2]))

def noise():
    global noise_step
    sigma = (1 - min(noise_step / 100000, 0.9))*1.0
    noise_step += 1
    n = np.random.normal(0.0, sigma, [1, 2])
    return n






# training

gen = FeedforwardGenerator(size)
agent_target = FeedforwardAgent()
agent = FeedforwardAgent(agent_target)
env = FeedforwardEnvironment(next(gen.generate()), size)
trainer = Trainer(env, agent)
decreasing_proba = lambda e, s: 1.0 / (1+2**((e-60)/10))
trainer.run(gen, batch_size=4, log_size=1024, explore_prob=0.0, noise=noise_callback, callback=draw_callback, verbose=False)



def generate_batch(size, board_size=5.0):
    initial_state = np.random.uniform(-board_size, board_size, [size, 4])
    player = initial_state[:, 0:2]
    dest = initial_state[:, 2:4]
    direction = dest-player
    lengths = np.sqrt(direction[:, 0:1]*direction[:, 0:1] + direction[:, 1:2]*direction[:, 1:2])
    direction /= lengths
    multiplier = [[1.0] if x[0]>1.0 else [x[0]] for x in lengths]
    direction *= multiplier

    return initial_state, direction

def state_to_numpy(state: FeedforwardEnvironmentState):
    numpy_state = np.concatenate([state.player, state.power_up], 1)
    return numpy_state

def numpy_to_action(numpy_direction):
    action = FeedforwardAction(numpy_direction)
    return action

#
#
# file = open("log_suprevised.txt", "wt")
# with tf.Graph().as_default() as graph:
#     op_x = tf.placeholder(tf.float32, [None, 4])
#     op_t = tf.placeholder(tf.float32, [None, 2])
#     op_h1 = tf.keras.layers.Dense(10, activation="tanh")(op_x)
#     op_h2 = tf.keras.layers.Dense(10, activation="tanh")(op_h1)
#     op_y = tf.keras.layers.Dense(2, activation="tanh")(op_h2)
#
#     loss = tf.losses.mean_squared_error(op_t,op_y)
#     minimize = tf.train.AdamOptimizer(0.1).minimize(loss)
#     init = tf.global_variables_initializer()
#
#     episode = 0
#     with tf.Session() as sess:
#         sess.run(init)
#
#         while True:
#
#             # act
#             gen = FeedforwardGenerator(size)
#             env = FeedforwardEnvironment(next(gen.generate()), size)
#
#             steps = 0
#             while not env.get_state().is_terminal() and episode>5:
#                 numpy_state = state_to_numpy(env.get_state())
#                 numpy_direction = sess.run(op_y, feed_dict={op_x: numpy_state})
#                 prob = decreasing_proba(episode, 0)
#
#                 #mutate
#                 if random.uniform(0.0, 1.0) < prob:
#                     numpy_direction += noise()
#
#                 action = numpy_to_action(numpy_direction)
#                 env.react(action)
#
#                 # draw
#                 player.set_offsets(numpy_state[0:, 0:2])
#                 power_up.set_offsets(numpy_state[0:, 2:4])
#                 plt.pause(0.05)
#
#                 # input prep
#                 rows = []
#                 for py in y:
#                     for px in x:
#                         rows.append([px, py, numpy_state[0, 2], numpy_state[0, 3]])
#                 batch = np.array(rows)
#
#                 # querying agent
#                 actions = sess.run(op_y, {op_x: batch})
#                 u, v = actions[:, 0].reshape([-1, len(x)]), actions[:, 1].reshape([-1, len(x)])
#
#                 # updating
#                 quiver.set_UVC(u, v)
#
#                 # update step
#                 steps += 1
#
#                 if steps> 1000:
#                     break
#
#             # save steps
#             file.write(str(steps)+"\n")
#             file.flush()
#
#             # train
#             batch_x, batch_y = generate_batch(32)
#             sess.run(minimize, {op_x: batch_x, op_t: batch_y})
#
#
#             episode += 1
#
#





#
# import numpy as np
# import matplotlib.pyplot as plt
# plt.ion()
# fig, ax = plt.subplots()
# collection = ax.scatter(np.arange(5), np.random.uniform(0, 1, [5]))
#
# for i in range(10):
#     y = np.random.random()
#     collection.set_offsets(np.random.uniform(0,1, [5,2]))
#     plt.pause(0.05)
