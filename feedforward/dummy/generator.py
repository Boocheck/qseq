from random import choice
from typing import Iterable

from feedforward.state import FeedforwardEnvironmentState
from framework.algorithm.stategenerator import StateGenerator
from framework.state import State
from sequence.state import SequenceEnvironmentState
import numpy as np


class FeedforwardGenerator(StateGenerator):

    def __init__(self, size):
        self._size = size

    def generate(self) -> Iterable[FeedforwardEnvironmentState]:
        while True:
            yield FeedforwardEnvironmentState(
                # np.array([[0.0, 0.0]]),
                # np.array([[4.0, 2.0]])
                np.random.uniform(-self._size, self._size, [1, 2]),
                np.random.uniform(-self._size, self._size, [1, 2])
            )
