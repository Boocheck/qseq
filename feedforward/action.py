from enum import Enum

from framework.action import Action
import numpy as np


class FeedforwardAction(Action):
    def __init__(self, data:np.ndarray=None):
        self.data = data

    def __repr__(self):
        return "<FeedforwardAction {!r}>".format(self.data)



