from typing import List
from framework.environment.state import EnvironmentState
import numpy as np


class FeedforwardEnvironmentState(EnvironmentState):

    @staticmethod
    def dist(a, b):
        return np.sqrt(np.sum(np.square(a - b)))


    def __init__(self, player: np.ndarray, power_up: np.ndarray):
        super().__init__()

        self.player = player
        self.power_up = power_up
        self.power_up_epsilon = 0.5



    def is_terminal(self) -> bool:
        return FeedforwardEnvironmentState.dist(self.player, self.power_up) < self.power_up_epsilon



    def __repr__(self):
        return "<FeedforwardEnvironmentState player {!r}, power up {!r}, dist {!r}>".format(
            self.player, self.power_up, FeedforwardEnvironmentState.dist(self.player, self.power_up)
        )
