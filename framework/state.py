from framework.agent.observation import Observation
from framework.agent.state import AgentState
from framework.environment.state import EnvironmentState


class State:
    def __init__(self, environment_state: EnvironmentState, agent_state: AgentState, observation: Observation):
        self._environment_state = environment_state
        self._agent_state = agent_state
        self._observation = observation

    def get_environment_state(self) -> EnvironmentState:
        return self._environment_state

    def get_agent_state(self) -> AgentState:
        return self._agent_state

    def get_observation(self) -> Observation:
        return self._observation

    def is_terminal(self) -> bool:
        return self._environment_state.is_terminal()



