from framework.environment.state import EnvironmentState


class StopCondition:
    def is_terminal(self, environment_state: EnvironmentState) -> bool:
        raise NotImplementedError()
