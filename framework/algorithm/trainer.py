from itertools import islice

from framework.agent.agent import Agent
from framework.algorithm.experience import Experience
from framework.algorithm.fixedlog import FixedExperienceLog
from framework.algorithm.stategenerator import StateGenerator
from framework.algorithm.stop import StopCondition
from framework.environment.environtment import Environment
from framework.environment.state import EnvironmentState
from framework.state import State


class Trainer:
    def __init__(self, environment: Environment, agent: Agent, stop_condition: StopCondition = None):
        self._environment = environment
        self._agent = agent
        self._stop_condition = stop_condition

    def run(self, state_generator: StateGenerator, verbose: bool = True, log_size=1000, batch_size=16, max_episodes = None, max_steps = None, explore_prob=0.0, noise=None, callback=None, log_path="log.txt"):

        file = open(log_path, "wt")

        experience_log = FixedExperienceLog(log_size)
        episode_iteration = 0

        for state in (state_generator.generate() if max_episodes is None else islice(state_generator.generate(), max_episodes)):
            if verbose:
                print("\n\n\n################################################# starting episode {}...".format(episode_iteration))

            self._environment.set_state(state)
            self._agent.reset()

            if verbose:
                print("initial env state {!r}".format(state))

            iteration = 0
            while not self._should_stop(self._environment.get_state()):
                if verbose:
                    print("####################### episod: {} step: {}".format(episode_iteration, iteration))

                before = State(self._environment.get_state(),
                               self._agent.get_state(),
                               self._agent.observe(self._environment.get_state()))
                # state = self._environment.get_state()
                if callable(explore_prob):
                    intent = self._agent.intend(before.get_observation(), explore=explore_prob(episode_iteration, iteration))
                else:
                    intent = self._agent.intend(before.get_observation(), explore=explore_prob)

                if noise is not None:
                    intent = noise(intent)

                action = self._agent.act(intent)
                environment_response = self._environment.react(action)
                after = State(self._environment.get_state(),
                              self._agent.get_state(),
                              self._agent.observe(self._environment.get_state()))

                experience = Experience(before, intent, action, environment_response.reward(), after)
                experience_log.add(experience)

                if verbose:
                    print(repr(experience))

                # TODO[boocheck]: revert to drawing batch from historical experiences
                self._agent.train(experience_log.random_batch(batch_size))

                iteration += 1

                if max_steps is not None:
                    if iteration > max_steps:
                        break

                if callback is not None:
                    callback(episode_iteration, iteration, self._agent, self._environment)

            if verbose:
                if max_steps is not None:
                    if iteration > max_steps:
                        print("breaking episode with env state: ")
                else:
                    print("episode finished with env state: ")
                print(repr(self._environment.get_state()))
                print()

            episode_iteration += 1

            file.write(str(iteration))
            file.write("\n")
            file.flush()


    def _should_stop(self, state: EnvironmentState):
        return self._stop_condition.is_terminal(state) if self._stop_condition is not None else state.is_terminal()
