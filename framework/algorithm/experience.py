from framework.action import Action
from framework.agent.intent import Intent
from framework.environment.reward import Reward
from framework.state import State


class Experience:
    def __init__(self, before: State, intent: Intent, action: Action, reward: Reward, after: State):
        self._before = before
        self._intent = intent
        self._action = action
        self._reward = reward
        self._after = after

    def get_before(self):
        return self._before

    def get_intent(self):
        return self._intent

    def get_action(self):
        return self._action

    def get_reward(self):
        return self._reward

    def get_after(self):
        return self._after

    def __repr__(self):
        return "<Experience \n\t" \
               "env: {!r}\n\t" \
               "obserervation: {!r}\n\t" \
               "action: {!r}\n\t" \
               "reward: {!r}\n\t" \
               "env: {!r}\n\t" \
               "observation: {!r}>".format(
            self._before.get_environment_state(),
            # self._before.get_agent_state(),
            self._before.get_observation(),
            self._action,
            self._reward,
            self._after.get_environment_state(),
            # self._after.get_agent_state(),
            self._after.get_observation(),
        )
