from typing import List

from framework.algorithm.experience import Experience


class ExperienceLog:
    def add(self, experience: Experience):
        raise NotImplementedError()

    def random_batch(self, size: int) -> List[Experience]:
        raise NotImplementedError()
