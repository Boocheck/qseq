from framework.agent.agent import Agent
from framework.agent.state import AgentState
from framework.environment.environtment import Environment
from framework.environment.state import EnvironmentState


class Runner:
    def __init__(self, environment: Environment, agent: Agent):
        self._environment = environment
        self._agent = agent

    def run(self, environment_state: EnvironmentState = None, agent_state: AgentState = None, verbose: bool = True):
        if environment_state is not None:
            self._environment.set_state(environment_state)

        if agent_state is not None:
            self._agent.set_state(agent_state)

        if verbose:
            print("starting runner...")

        iteration = 0
        while not self._environment.get_state().is_terminal():
            if verbose:
                print("perform step: {}".format(iteration))

            state = self._environment.get_state()
            action = self._agent.act(state)
            self._environment.react(action)
            iteration += 1

        if verbose:
            print("runner finished")
