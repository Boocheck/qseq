from random import choices
from typing import List

from framework.algorithm.experience import Experience
from framework.algorithm.experiencelog import ExperienceLog


class FixedExperienceLog(ExperienceLog):
    def __init__(self, size: int):
        self._size = size
        self._storage = []

    def add(self, experience: Experience):
        self._storage.append(experience)
        if len(self._storage) > self._size:
            del self._storage[0]

    def random_batch(self, size: int) -> List[Experience]:
        return choices(self._storage, k=size)
