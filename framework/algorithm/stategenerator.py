from typing import Iterable

from framework.environment.state import EnvironmentState


class StateGenerator:
    def generate(self) -> Iterable[EnvironmentState]:
        raise NotImplementedError
