from typing import List

from framework.action import Action
from framework.agent.intent import Intent
from framework.agent.observation import Observation
from framework.agent.state import AgentState
from framework.algorithm.experience import Experience
from framework.environment.state import EnvironmentState


class Agent:
    def __init__(self, agent_state: AgentState = None):
        self._agent_state = self._create_initial_state() if agent_state is None else agent_state

    def act(self, intent: Intent) -> Action:
        raise NotImplementedError()

    def intend(self, observation: Observation, explore: float = 0.0) -> Intent:
        intent, agent_state = self.try_intend(observation, explore=explore)
        self._agent_state = agent_state
        return intent

    def try_intend(self, observation: Observation, explore: float = 0.0) -> (Intent, AgentState):
        raise NotImplementedError()

    def train(self, experience_batch: List[Experience]):
        raise NotImplementedError()

    def observe(self, environment_state: EnvironmentState) -> Observation:
        raise NotImplementedError()

    def reset(self):
        self._agent_state = self._create_initial_state()

    def get_state(self):
        return self._agent_state

    def set_state(self, agent_state: AgentState):
        self._agent_state = agent_state

    def _create_initial_state(self):
        raise NotImplementedError()
