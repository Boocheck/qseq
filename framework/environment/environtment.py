from framework.action import Action
from framework.environment.response import EnvironmentResponse
from framework.environment.state import EnvironmentState


class Environment:
    def __init__(self, environment_state: EnvironmentState):
        self._environment_state = environment_state

    def react(self, action: Action) -> EnvironmentResponse:
        raise NotImplementedError()

    def validate(self, action: Action) -> bool:
        raise NotImplementedError()

    def get_state(self) -> EnvironmentState:
        return self._environment_state

    def set_state(self, environment_state: EnvironmentState):
        self._environment_state = environment_state
