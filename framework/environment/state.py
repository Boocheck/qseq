class EnvironmentState:
    def is_terminal(self) -> bool:
        raise NotImplementedError()
