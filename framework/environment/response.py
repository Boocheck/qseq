from framework.environment.reward import Reward
from framework.environment.state import EnvironmentState


class EnvironmentResponse:
    def __init__(self, environment_state: EnvironmentState, reward: Reward):
        self._environment_state = environment_state
        self._reward = reward

    def environment_state(self) -> EnvironmentState:
        return self._environment_state

    def reward(self) -> Reward:
        return self._reward
