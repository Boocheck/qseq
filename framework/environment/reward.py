class Reward:
    def __init__(self, value: float):
        self._value = value

    def get_value(self) -> float:
        return self._value

    def __repr__(self):
        return "<Reward {}>".format(self._value)

