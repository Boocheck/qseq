from typing import List

from framework.environment.state import EnvironmentState


class SequenceEnvironmentState(EnvironmentState):
    def __init__(self, image: List[float], image_index: int, bits: List[float], bits_index: int, decoded: List[float],
                 decoded_index: int):
        super().__init__()

        self.image = image
        self.image_index = image_index
        self.bits = bits
        self.bits_index = bits_index
        self.decoded = decoded
        self.decoded_index = decoded_index

    def is_terminal(self) -> bool:
        return self.decoded_index >= len(self.decoded)

    def __repr__(self):
        return "<SequenceEnvironmentState {}, {}, {}>".format(
            self._list_index_printer(self.image, self.image_index),
            self._list_index_printer(self.bits, self.bits_index),
            self._list_index_printer(self.decoded, self.decoded_index),
        )

    def _list_index_printer(self, alist, ind):
        result = "["
        result += ", ".join([">" + repr(x) + "<" if i == ind else repr(x) for i, x in enumerate(alist)])
        result += "]"
        if ind >= len(alist):
            result += " ><"

        return result
