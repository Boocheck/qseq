import numpy as np

from framework.agent.observation import Observation


class DecoderObservation(Observation):
    def __init__(self, bits_window: np.ndarray, bits_finished: np.ndarray):
        self.bits_window = bits_window
        self.bits_finished = bits_finished

    def __repr__(self):
        return "<DecoderObservation bits_window: {!r} bits_finished: {!r}".format(self.bits_window, self.bits_finished)
