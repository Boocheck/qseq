import random
from typing import List

import datetime
import numpy as np
import tensorflow as tf

from framework.action import Action
from framework.agent.intent import Intent
from framework.agent.observation import Observation
from framework.agent.state import AgentState
from framework.algorithm.experience import Experience
from framework.algorithm.stop import StopCondition
from framework.environment.state import EnvironmentState
from framework.state import State
from sequence.action import SequenceAction, ActionType
from sequence.agent import SequenceAgent
from sequence.decoder.intent import DecoderIntent
from sequence.decoder.observation import DecoderObservation
from sequence.state import SequenceEnvironmentState
from utils.combining import destructuring, combine


class DecoderAgent(SequenceAgent):
    def __init__(self, state_size: int, dense_size:int, agent_state: "DecoderAgentState" = None, stop_condition: StopCondition = None, target_model:"DecoderAgent"=None, tau=0.001):

        self._state_size = state_size
        self._dense_size = dense_size
        self._stop_condition = stop_condition
        self._target_model = target_model
        self._tau = tau

        self._graph = tf.Graph()
        self._session = tf.Session(graph=self._graph)
        self._discount_factor = 0.99
        self._train_iter = 0

        with self._graph.as_default():
            with tf.variable_scope("decoder") as coder_scope:
                self._input = tf.placeholder(tf.float32, [None, 2], name="decoder_input")
                self._q_target = tf.placeholder(tf.float32, [None, 1], name="decoder_q_target")

                with tf.variable_scope("actor") as vs:
                    self._actor_cell = tf.contrib.rnn.LSTMCell(self._state_size)
                    self._actor_state = [tf.placeholder(tf.float32, [None, self._state_size]) for _ in range(2)]
                    self._actor_cell_output, self._actor_new_state = tf.contrib.rnn.static_rnn(self._actor_cell,
                                                                                               [self._input],
                                                                                               initial_state=self._actor_state)
                    # TODO[boocheck]: remove this temporary disabling lstm
                    # self._actor_cell_output = [tf.layers.dense(self._input, 10, tf.tanh)]
                    self._actor_dense1 = tf.layers.dense(self._actor_cell_output[0], dense_size, tf.tanh)
                    self._actor_dense1 = tf.layers.dense(self._actor_dense1, dense_size, tf.tanh)
                    self._actor_output_probas = tf.layers.dense(self._actor_dense1, 2, tf.nn.softmax)
                    self._actor_dense2 = tf.layers.dense(self._actor_dense1, 2, tf.sigmoid)
                    self._actor_output_image = tf.layers.dense(tf.concat((self._actor_output_probas, self._actor_dense2), 1), 1, tf.sigmoid)
                    # TODO[boocheck]: establish if random noise on action helps
                    self._actor_output = tf.concat((self._actor_output_probas, self._actor_output_image), 1)
                    self._actor_output += tf.random_normal(tf.shape(self._actor_output), 0.0, 0.1)

                    self._actor_variables = [v for v in tf.global_variables() if
                                             vs.name in v.name and coder_scope.name in v.name]



                with tf.variable_scope("critic") as vs:
                    self._critic_input = tf.concat([self._actor_output, self._input], 1)
                    self._critic_cell = tf.contrib.rnn.LSTMCell(self._state_size)
                    self._critic_state = [tf.placeholder(tf.float32, [None, self._state_size]) for _ in range(2)]
                    self._critic_cell_output, self._critic_new_state = tf.contrib.rnn.static_rnn(self._critic_cell,
                                                                                                 [self._critic_input],
                                                                                                 initial_state=self._critic_state)
                    # TODO[boocheck]: remove this temporary disabling lstm
                    # self._critic_cell_output = [tf.layers.dense(self._critic_input, 10, tf.tanh)]
                    self._critic_dense1 = tf.layers.dense(self._critic_cell_output[0], self._dense_size, tf.tanh )
                    self._critic_dense1 = tf.layers.dense(self._critic_dense1, self._dense_size, tf.tanh)
                    self._critic_output = tf.layers.dense(self._critic_dense1, 1)
                    self._critic_variables = [v for v in tf.global_variables() if
                                              vs.name in v.name and coder_scope.name in v.name]

                self._actor_minimize = tf.train.AdamOptimizer(0.0001).minimize(tf.reduce_mean(-self._critic_output),
                                                                               var_list=self._actor_variables)
                self._critic_minimize = tf.train.AdamOptimizer(0.001).minimize(
                    tf.reduce_mean(tf.square(self._critic_output - self._q_target)), var_list=self._critic_variables)

                self.initializer = tf.global_variables_initializer()

            tf.summary.tensor_summary("input", self._input)
            tf.summary.tensor_summary("actor_state", self._actor_state)
            tf.summary.tensor_summary("critic_state", self._critic_state)
            self._summaries = tf.summary.merge_all()

        super().__init__(agent_state)
        self._session.run(self.initializer)
        if target_model is not None:
            target_model.sync_to(self)
        self._writer = tf.summary.FileWriter("logdir/" + datetime.datetime.now().isoformat("_").replace(":", "_"), self._graph)



    def act(self, intent: Intent) -> Action:
        if not isinstance(intent, DecoderIntent):
            raise ValueError()

        probas = intent.decoder_intent[:, 0:2].flatten()
        probas = np.maximum(probas, 0)
        probas /= probas.sum()
        action_idx = np.random.choice(2, p=probas)
        # action_idx = np.argmax(probas)

        if action_idx == 0:
            action = SequenceAction(ActionType.WRITE_ELEM, np.asscalar(intent.decoder_intent[:, 2]))
        elif action_idx == 1:
            action = SequenceAction(ActionType.NEXT_BIT, None)
        else:
            raise ValueError()

        print("action")
        print(intent.decoder_intent)
        print(action)

        return action

    def train(self, experience_batch: List[Experience]):
        dict_before = self._prepare_batch([x.get_before() for x in experience_batch])
        dict_after = self._prepare_batch([x.get_after() for x in experience_batch])
        dict_after_target = self._target_model._prepare_batch([x.get_after() for x in experience_batch])
        rewards = np.array([e.get_reward().get_value() for e in experience_batch]).reshape([-1,1])
        terminal = np.array([(0. if self._stop(e.get_after().get_environment_state()) else 1.) for e in experience_batch]).reshape([-1, 1])

        q_before = self._session.run(self._critic_output, dict_before)  # TODO[boocheck]: remove this
        q_after = self._target_model._session.run(self._target_model._critic_output, dict_after_target)
        q_target = rewards + terminal * self._discount_factor * q_after

        #     np.array([[rewards[i] + self._discount_factor * (
        # 0 if self._stop(experience_batch[i].get_after().get_environment_state()) else 1) * q_after[i, 0]]
        #                      for i in range(len(experience_batch))])

        dict_before_with_q = dict_before.copy()
        dict_before_with_q.update({
            self._q_target: q_target,
            self._actor_output: combine(experience_batch, lambda x: x.get_intent().decoder_intent)
        })

        print("### before training: ###")
        print("before|after|target|diff")
        print(np.concatenate((q_before, q_after, q_target, q_target - q_before), 1))
        print()




        _, actor_summ = self._session.run((self._critic_minimize, self._summaries), dict_before_with_q)



        # TODO[boocheck]: remove this
        q_before = self._session.run(self._critic_output, dict_before)  # TODO[boocheck]: remove this
        q_after = self._session.run(self._critic_output, dict_after)
        q_target = rewards + terminal * self._discount_factor * q_after

        print("### after critic: ###")
        print("before|after|target|diff")
        print(np.concatenate((q_before, q_after, q_target, q_target - q_before), 1))
        print()




        self._session.run(self._actor_minimize, dict_before)





        # TODO[boocheck]: remove this
        q_before = self._session.run(self._critic_output, dict_before)  # TODO[boocheck]: remove this
        q_after = self._session.run(self._critic_output, dict_after)
        q_target = rewards + terminal * self._discount_factor * q_after

        print("### after critic and actor: ###")
        print("before|after|target|diff")
        print(np.concatenate((q_before, q_after, q_target, q_target - q_before), 1))
        print()

        self._target_model.sync_to(self, False, 0.01)

        self._writer.add_summary(actor_summ, self._train_iter)
        self._writer.flush()
        self._train_iter += 1


    def observe(self, environment_state: EnvironmentState) -> Observation:
        s = environment_state
        if not isinstance(s, SequenceEnvironmentState):
            raise ValueError()

        return DecoderObservation(
            np.array([[s.bits[s.bits_index]]]) if s.bits_index < len(s.bits) else np.array([[0]]),
            np.array([[0]]) if s.bits_index < len(s.bits) else np.array([[1]]),
        )

    def try_intend(self, observation: Observation) -> (Intent, AgentState):
        if not isinstance(observation, DecoderObservation):
            raise ValueError()

        # TODO[boocheck]: try random action with some probability
        # if random.random()<0.3:
        #     prob1 = random.random()
        #     prob2 = 1.0-prob1
        #     prob3 = random.random()
        #
        #     print("RANDOM RANDOM RANDOM")
        #     return DecoderIntent(np.array([[prob1, prob2, prob3]])), self._agent_state

        actor_output, actor_state, critic_state = self._session.run(
            (self._actor_output, self._actor_new_state, self._critic_new_state),
            {
                self._input: np.concatenate((observation.bits_window, observation.bits_finished), 1),
                self._actor_state[0]: self._agent_state.actor_state[0],
                self._actor_state[1]: self._agent_state.actor_state[1],
                self._critic_state[0]: self._agent_state.critic_state[0],
                self._critic_state[1]: self._agent_state.critic_state[1]
            }

        )

        return DecoderIntent(actor_output), DecoderAgentState(actor_state, critic_state)

    def _create_initial_state(self):
        a, c = [[np.zeros([1, self._state_size]) for x in range(2)] for y in range(2)]
        return DecoderAgentState(a, c)

    def _prepare_batch(self, batch: List[State]):
        observations = [self.observe(x.get_environment_state()) for x in batch]

        batch_dict = destructuring({
            self._input: lambda: combine(observations, lambda x: np.concatenate((x.bits_window, x.bits_finished), 1)),
            self._actor_state[0]: lambda: combine(batch, lambda x: x.get_agent_state().actor_state[0]),
            self._actor_state[1]: lambda: combine(batch, lambda x: x.get_agent_state().actor_state[1]),
            self._critic_state[0]: lambda: combine(batch, lambda x: x.get_agent_state().critic_state[0]),
            self._critic_state[1]: lambda: combine(batch, lambda x: x.get_agent_state().critic_state[1])
        })

        return batch_dict

    def _stop(self, environment_state: EnvironmentState):
        return self._stop_condition.is_terminal(
            environment_state) if self._stop_condition is not None else environment_state.is_terminal()

    def close(self):
        self._writer.close()


class DecoderAgentState(AgentState):
    def __init__(self, actor_state, critic_state):
        self.actor_state = actor_state
        self.critic_state = critic_state


    def __repr__(self):
        return "<DecoderAgentState actor: {!r} critic: {!r}>".format(self.actor_state, self.critic_state)
