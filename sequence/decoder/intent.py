import numpy as np

from framework.agent.intent import Intent


class DecoderIntent(Intent):
    def __init__(self, intent: np.ndarray):
        self.decoder_intent = intent
