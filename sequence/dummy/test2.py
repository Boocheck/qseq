from numpy.random import binomial
from scipy.stats import binom
import numpy as np

import matplotlib.pyplot as plt



def bet(number_of_matches, stake, win_prob):

    probs = binom.pmf(np.arange(number_of_matches+1), number_of_matches, win_prob)
    vals = stake * np.arange(number_of_matches+1)

    almost_expected_value = probs*vals
    expected_value = np.sum(almost_expected_value)

    return expected_value-number_of_matches


def find_win_prob(number_of_matches, stake):
    win_prob = 0.0
    while True:
        if bet(number_of_matches, stake, win_prob)>0:
            return win_prob

        win_prob+=0.001



def find_stake(number_of_matches, win_prob):
    stake = 0.0
    while True:
        if bet(number_of_matches, stake, win_prob)>0:
            return stake

        stake+=0.001


# print(find_stake(300, 0.5))
print(find_win_prob(20, 3.0))

#
#
# #
# # plt.plot(binom.pmf(np.arange(12+1), 12, 1/12))
# # plt.show()


#
#
#
#
# def play(games=1, p=1000, a=1000):
#     for _ in range(games):
#         res = np.random.randint(1, 5)
#         if res > 1:
#             p-=3
#             a+=3
#         else:
#             a-=10
#             p+=10
#
#     return {"a": a, "p": p}
#
#
# print(play(10000000))
