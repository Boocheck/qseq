import matplotlib.pyplot as plt

with open("mses.txt", "rt") as f:
    lines = f.readlines()

floats = [float(x) for x in lines]


plt.plot(floats)
plt.show()