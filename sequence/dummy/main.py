from framework.algorithm.trainer import Trainer
from sequence.coder.agent import CoderAgent
from sequence.decoder.agent import DecoderAgent
from sequence.dummy.generator import DummyGenerator
from sequence.dummy.stop import CoderDummyStop, DecoderDummyStop
from sequence.environment import SequenceEnvironment

import tensorflow
print(tensorflow.__version__)


gen = DummyGenerator()
stop_coder = CoderDummyStop()
stop_decoder = DecoderDummyStop()
coder = CoderAgent(16, stop_condition=stop_coder)
decoder_target = DecoderAgent(8,256, stop_condition=stop_decoder)
decoder = DecoderAgent(8, 256, stop_condition=stop_decoder, target_model=decoder_target)

env = SequenceEnvironment(next(gen.generate()))

# state = env.get_state()
# action = decoder.act(state)
# action2 = decoder2.act(state)
# print()
#
# action = decoder.act(state)
# decoder2.clone(decoder)
# action2 = decoder2.act(state)
# print()
#
# decoder_state = decoder.get_state()
# action = decoder.act(state)
# decoder.set_state(decoder_state)
# action = decoder.act(state)
# decoder2.set_state(decoder_state)
# decoder2.clone(decoder)
# action2 = decoder2.act(state)
#


trainer = Trainer(env, decoder, stop_decoder)
trainer.run(gen, batch_size=64, log_size=1000000)
decoder.close()

