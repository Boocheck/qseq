from multiprocessing.pool import Pool
import numpy as np

# zadanie: rozdzielic liczby z corpus na basket_num koszykow w zaleznosci od reszty z dzielenia przez basket_num
baskets_num = 7
corpus = np.random.randint(0, 1000000, 1000000).tolist()


def mapper(number):
    result = [[]]*baskets_num
    result[number%baskets_num].append(number)
    return result


if __name__ == "__main__":
    p = Pool(16)
    mapped = p.map(mapper, corpus)
    merged = list(zip(*mapped))
    x = 1