import tensorflow as tf
import numpy as np



def batch(bsize, ssize, esize):
    inp = np.random.uniform(0,1, [bsize, ssize, esize])
    out = np.cumsum(inp, axis=1)
    return inp, out


bshape = bsize, ssize, esize = [16, 30, 1]
ph_in = tf.placeholder(tf.float32, bshape)
ph_out = tf.placeholder(tf.float32, bshape)
h = tf.keras.layers.LSTM(100, return_sequences=True)(ph_in)
h2 = tf.keras.layers.LSTM(100, return_sequences=True)(h)
o = tf.keras.layers.Dense(1)(h2)

loss = tf.losses.mean_squared_error(ph_out, o)
minimize = tf.train.AdamOptimizer().minimize(loss)

init = tf.global_variables_initializer()


with tf.Session() as sess:
    sess.run(init)
    while(True):
        inp, out = batch(*bshape)
        l, _ = sess.run([loss, minimize], {ph_in: inp, ph_out: out})
        print("{:6.3}".format(l))








