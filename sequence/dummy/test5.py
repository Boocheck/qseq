import datetime
import math

from tensorflow import python as tf
import numpy as np
import random
import matplotlib.pyplot as plt


# mnist = tf.keras.datasets.mnist
#
# (x_train, y_train), (x_test, y_test) = mnist.load_data()
# x_train, x_test = x_train / 255.0, x_test / 255.0
#
#
#
# model = tf.keras.models.Sequential([
#     tf.keras.layers.Flatten(input_shape=(28, 28)),
#     tf.keras.layers.Dense(128, activation='relu'),
#     tf.keras.layers.Dropout(0.2),
#     tf.keras.layers.Dense(10, activation='softmax')
# ])
#
# model.compile(optimizer='adam',
#               loss='sparse_categorical_crossentropy',
#               metrics=['accuracy'])
#
# model.fit(x_train, y_train, epochs=5)
# model.evaluate(x_test, y_test)

def array(*args, **kwargs):
    kwargs.setdefault("dtype", np.float32)
    return np.array(*args, **kwargs)


# SETTINGS
EPISODE_LENGTH = 32
CODER_INPUT_SIZE = 2
DECODER_INPUT_SIZE = 2


class RewardPolicy:
    def __init__(self):
        self.env = None

    def coder_reads_data(self):
        return 0.0

    def coder_reads_finished_stream(self):
        return -1.0

    def coder_writes_data(self):
        return -abs(self.env.code[len(self.env.code) - 1] - self.env.source[len(self.env.code) - 1]) if len(
            self.env.code) <= len(self.env.source) else -1.0

    def coder_writes_eos(self):
        # return 0.0
        # return 0.0
        return -self.calc_difference(self.env.source, self.env.code)
        # return -self.calc_difference()

    def decoder_reads_data(self):
        return 0.0

    def decoder_reads_finished_stream(self):
        return -1.0

    def decoder_writes_data(self):
        return 0.0

    def decoder_writes_eos(self):
        return -self.calc_difference(self.env.source, self.env.dest)

    def calc_difference(self, a, b):
        diff = 0.0
        for i in range(min(len(a), len(b))):
            diff += abs(a[i] - b[i])
        diff += abs(len(a) - len(b))
        return diff

class EnvEasy:
    def __init__(self, initial_state, size=5):
        self.state = initial_state
        self.size = size
        self.epsilon = 0.1

    def act(self, action):
        action = action[:, 0, :]
        action_len = np.linalg.norm(action)
        action = np.where(action_len > 1.0, action/action_len, action)

        position = self.state[:, 0:2]
        velocity = self.state[:, 2:4]

        velocity += action
        new_position = np.clip(position + velocity,  -self.size, self.size)

        dist_change = np.linalg.norm(position, axis=1)[0] - np.linalg.norm(new_position, axis=1)[0]

        self.state = np.concatenate([new_position, velocity], 1)

        return dist_change.reshape([1,1,1])-0.01

    def observe(self):
        return self.state[:, 0:2].reshape([-1,1,2])

    def is_finished(self):
        return np.linalg.norm(self.state[:, 0:2], axis=1) < 1.0

    def not_terminal(self):
        return np.array([[[1.0]]]) if not self.is_finished() else np.array([[[0.0]]])



class EnvEncoding:
    def __init__(self, state, reward_policy: RewardPolicy):
        self.reward_policy = reward_policy
        self.source = state.flatten().tolist()
        self.source_ind = 0
        self.code = []
        self.code_ind = 0
        self.code_finished = False
        self.dest = []
        self.dest_finished = False
        self.coder_reward_listeners = []
        self.decoder_reward_listeners = []

    def emit_coder_reward(self, reward):
        for listener in self.coder_reward_listeners:
            listener(array([[[reward]]]))

    def emit_decoder_reward(self, reward):
        for listener in self.decoder_reward_listeners:
            listener(array([[[reward]]]))

    def coder_observe(self):
        res = [[[self.source[self.source_ind], 1.0] if self.source_ind < len(self.source) else [0.0, 0.0]]]
        return array(res)

    def coder_act(self, action):
        action = action.flatten()
        ind = np.argmax(action[1:])
        val = action[0]
        if ind == 0:  # read
            if self.source_ind > len(self.source):
                self.emit_coder_reward(self.reward_policy.coder_reads_finished_stream())
                return "coder_reads_finished_stream"
            else:
                self.source_ind += 1
                self.emit_coder_reward(self.reward_policy.coder_reads_data())
                return "coder_reads_data"
        elif ind == 1:  # write
            self.code.append(val)
            self.emit_coder_reward(self.reward_policy.coder_writes_data())
            return "coder_writes_data"
        elif ind == 2:  # eos
            self.code_finished = True
            self.emit_coder_reward(self.reward_policy.coder_writes_eos())
            return "coder_writes_eos"

    def decoder_observe(self):
        res = [[[self.code[self.code_ind], 1.0] if self.code_ind < len(self.code) else [0.0, 0.0]]]
        return array(res)

    def decoder_act(self, action):
        action = action.flatten()
        ind = np.argmax(action[1:])
        val = action[0]
        if ind == 0:  # read
            if self.code_ind > len(self.code):
                self.emit_decoder_reward(self.reward_policy.decoder_reads_finished_stream())
            else:
                self.code_ind += 1
                self.emit_decoder_reward(self.reward_policy.decoder_reads_data())
        elif ind == 1:  # write
            self.dest.append(val)
            self.emit_decoder_reward(self.reward_policy.decoder_writes_data())
        elif ind == 2:  # eos
            self.dest_finished = True
            self.emit_decoder_reward(self.reward_policy.decoder_writes_eos())
            # self.emit_coder_reward(self.reward_policy.coder_writes_eos())

    def __str__(self):
        return self._print_array_with_index(self.source, self.source_ind) + "\n" \
               + self._print_array_with_index(self.code, self.code_ind) + "\n" \
               + self._print_array_with_index(self.dest, 0) + "\n"

    def _print_array_with_index(self, arr, ind, format="{:.5f}"):

        result = [">" + format.format(x) + "<" if ind == i else format.format(x) for i, x in enumerate(arr)]
        result = "[" + ", ".join(result) + "]" + (" ><" if ind >= len(arr) else "")
        return result


class Agent:

    def __init__(self, layers):
        self.layers = layers

    def clone_from(self, other_agent, factor=1.0):
        for i in range(len(self.layers)):
            self_weights = self.layers[i].trainable_variables
            other_weights = self.layers[i].trainable_variables
            for j in range(len(self_weights)):
                self_weights[j].assign((1.0 - factor) * self_weights[j] + factor * other_weights[j])


class RecurrentAgent(Agent):

    def __init__(self, layers, recurrent_layers):
        super().__init__(layers)
        self.recurrent_layers = recurrent_layers
        self.recurrent_states = [None for _ in self.recurrent_layers]

    def reset(self):
        for i, layer in enumerate(self.recurrent_layers):
            self.recurrent_states[i] = None


class ActorCritic(RecurrentAgent):

    def __init__(self, action_size):
        self.coder_actor_dense1_layer = tf.keras.layers.Dense(2, activation="tanh")
        self.coder_actor_dense2_layer = tf.keras.layers.Dense(2, activation="tanh")
        self.coder_actor_gru1_layer = tf.keras.layers.LSTM(2, return_sequences=True, return_state=True)
        # self.coder_actor_result_layer = tf.keras.layers.Dense(action_size, activation="tanh")
        # self.coder_actor_gru2_layer = tf.keras.layers.LSTM(4, return_sequences=True, return_state=True, activation=None)

        self.coder_critic_adapter = tf.keras.layers.Dense(2, activation="tanh")
        self.coder_critic_dense1_layer = tf.keras.layers.Dense(2, activation="tanh")
        self.coder_critic_dense2_layer = tf.keras.layers.Dense(2, activation="tanh")
        self.coder_critic_gru1_layer = tf.keras.layers.LSTM(1, return_sequences=True, return_state=True)
        # self.coder_critic_gru2_layer = tf.keras.layers.LSTM(64, return_sequences=True, return_state=True)
        # self.coder_critic_gru3_layer = tf.keras.layers.LSTM(1, return_sequences=True, return_state=True, activation="tanh")
        # self.coder_critic_result_layer = tf.keras.layers.Dense(1, activation="tanh")

        recurrent_layers = [self.coder_actor_gru1_layer,
                            # self.coder_actor_gru2_layer,
                            self.coder_critic_gru1_layer,
                            # self.coder_critic_gru2_layer,
                            # self.coder_critic_gru3_layer
                            ]

        not_recurrent_layers = [self.coder_actor_dense1_layer,
                                self.coder_actor_dense2_layer,
                                # self.coder_actor_result_layer,
                                self.coder_critic_adapter,
                                self.coder_critic_dense1_layer,
                                self.coder_critic_dense2_layer,
                                # self.coder_critic_result_layer
                                ]
        super().__init__(recurrent_layers + not_recurrent_layers, recurrent_layers)

    @tf.function
    def actor_graph(self, env_state, state):
        a = self.coder_actor_dense1_layer(env_state)
        a = self.coder_actor_dense2_layer(a)
        a_and_states = self.coder_actor_gru1_layer(a, initial_state=state)
        a, new_state = a_and_states[0], a_and_states[1:]
        # a = self.coder_actor_result_layer(a)
        # a_and_states = self.coder_actor_gru2_layer(a, initial_state=self.recurrent_states[1])
        # a, self.recurrent_states[1] = a_and_states[0], a_and_states[1:]

        # TODO: uncomment for decoding problem
        # pixels = tf.keras.activations.sigmoid(a[:, :, 0:1])
        # action_type = tf.keras.activations.softmax(a[:, :, 1:4])
        # a = tf.keras.layers.concatenate([pixels, action_type])
        return a, new_state

    def actor(self, env_state):
        state = self.recurrent_states[0] if self.recurrent_states[0] is not None else self.coder_actor_gru1_layer.get_initial_state(env_state)
        a, new_state = self.actor_graph(env_state, state)
        self.recurrent_states[0] = new_state
        return a

    def actor_variables(self):
        res = []
        res += self.coder_actor_dense1_layer.trainable_variables
        res += self.coder_actor_dense2_layer.trainable_variables
        res += self.coder_actor_gru1_layer.trainable_variables
        # res += self.coder_actor_result_layer.trainable_variables
        return res

    @tf.function
    def critic_graph(self, env_state, action, state):
        ad = self.coder_critic_adapter(action)
        c = self.coder_critic_dense1_layer(env_state)
        c = self.coder_critic_dense2_layer(c)
        c_and_states = self.coder_critic_gru1_layer(c + ad, initial_state=state)
        c, new_state = c_and_states[0], c_and_states[1:]
        # c = self.coder_critic_result_layer(c)
        # c_and_states = self.coder_critic_gru2_layer(c, initial_state=self.recurrent_states[3])
        # c, self.recurrent_states[3] = c_and_states[0], c_and_states[1:]
        # c_and_states = self.coder_critic_gru3_layer(ad + c, initial_state=self.recurrent_states[4])
        # c, self.recurrent_states[4] = c_and_states[0], c_and_states[1:]
        c *= 32.0
        return c, new_state

    def critic(self, env_state, action):
        state = self.recurrent_states[1] if self.recurrent_states[1] is not None else self.coder_critic_gru1_layer.get_initial_state(env_state)
        c, new_state = self.critic_graph(env_state, action, state)
        self.recurrent_states[1] = new_state
        return c

    def critic_variables(self):
        res = []
        res += self.coder_critic_adapter.trainable_variables
        res += self.coder_critic_dense1_layer.trainable_variables
        res += self.coder_critic_dense2_layer.trainable_variables
        res += self.coder_critic_gru1_layer.trainable_variables
        # res += self.coder_critic_result_layer.trainable_variables
        # res += self.coder_critic_gru2_layer.trainable_variables
        # res += self.coder_critic_gru3_layer.trainable_variables
        return res


class Decoder:
    def __init__(self):
        self.decoder_actor_gru1_layer = tf.keras.layers.GRU(10, return_sequences=True, return_state=True)
        self.decoder_actor_gru2_layer = tf.keras.layers.GRU(3, return_sequences=True, return_state=True)
        self.decoder_actor_adapter = tf.keras.layers.Dense(10)
        self.decoder_critic_gru1_layer = tf.keras.layers.GRU(10, return_sequences=True, return_state=True)
        self.decoder_critic_gru2_layer = tf.keras.layers.GRU(10, return_sequences=True, return_state=True)
        self.decoder_critic_gru3_layer = tf.keras.layers.GRU(1, return_sequences=True, return_state=True)
        self.recurrent_layers = [self.decoder_actor_gru1_layer,
                                 self.decoder_actor_gru2_layer,
                                 self.decoder_critic_gru1_layer,
                                 self.decoder_critic_gru2_layer,
                                 self.decoder_critic_gru3_layer]
        self.recurrent_states = [None for _ in self.recurrent_layers]
        super().__init__(self.recurrent_layers + [self.decoder_actor_adapter])

    def call(self, x):
        a = self.decoder_actor_gru1_layer(x)
        a = self.decoder_actor_gru2_layer(a)
        ad = self.decoder_actor_adapter(a)
        c = self.decoder_critic_gru1_layer(x)
        c = self.decoder_critic_gru2_layer(c)
        q = self.decoder_critic_gru3_layer(ad + c)
        actor_variables = [self.decoder_actor_gru1_layer.trainable_variables,
                           self.decoder_actor_gru2_layer.trainable_variables]
        critic_variables = [self.decoder_critic_gru1_layer.trainable_variables,
                            self.decoder_critic_gru2_layer.trainable_variables,
                            self.decoder_critic_gru3_layer.trainable_variables,
                            self.decoder_actor_adapter.trainable_variables]
        return a, q, actor_variables, critic_variables


class EpisodeLog(list):

    def __init__(self, log_size):
        super().__init__()
        self._log_size = log_size
        self._dict = {}

    def append(self, p_object):
        example_length = p_object[1].shape[1]

        super().append(p_object)
        if example_length not in self._dict.keys():
            self._dict[example_length] = []
        self._dict[example_length].append(p_object)

        if len(self) > self._log_size:
            deleted = self.pop(0)
            deleted_length = deleted[1].shape[1]
            containing_list = self._dict[deleted_length]
            containing_list.remove(deleted)
            if len(self._dict[deleted_length]) == 0:
                del self._dict[deleted_length]

    def prepare_batch(self, target_model, batch_size, batch_length=None, error_mask=None):
        if batch_length is None:
            # bucketing
            key = random.choice(list(self._dict.keys()))
            batch_size = min(batch_size, len(self._dict[key]))
            chosen = random.sample(self._dict[key], k=batch_size)

            states_conc = np.concatenate([x[0] for x in chosen], 0)
            actions_conc = np.concatenate([x[1] for x in chosen], 0)
            rewards_conc = np.concatenate([x[2] for x in chosen], 0)
            not_terminal_conc = np.concatenate([x[3] for x in chosen], 0)

        else:
            # cropping
            raise NotImplemented()



        targets = prepare_targets(states_conc, rewards_conc, not_terminal_conc, target_model)

        if error_mask is None:
            result = (states_conc[:, :-1], actions_conc, rewards_conc), targets
        else:
            result = (states_conc[:, :-1], actions_conc, rewards_conc), targets, generate_error_mask(actions_conc.shape, error_mask)

        return result

    def dict(self):
        return self._dict


def prepare_targets(states, rewards, not_terminal, target_model):
    target_model.reset()
    qs = target_model.critic(states, target_model.actor(states)).numpy()
    targets = rewards + 0.99 * (qs * not_terminal)[:, 1:]
    return targets

def generate_error_mask(shape, gamma):
    mask = np.power(np.linspace([0.0]*shape[0], [1.0]*shape[0], shape[1], axis=-1), gamma)

    return mask



def print_batch(sar, t, model: ActorCritic, action_size=2):
    model.reset()

    actions = model.actor(sar[0])
    q = model.critic(sar[0], actions)

    abs_g_t = np.abs(q - t)
    mean_abs_q_t = np.mean(abs_g_t)

    abs_action = np.abs(actions - sar[1])
    mean_abs_action = np.mean(abs_action)

    result = np.concatenate([sar[0], sar[2], t, q, abs_g_t, sar[1], actions, abs_action], -1)
    labels = ["state"] * 2 + ["reward", "q_t", "q", "q_diff"] + ["a_t"] * action_size + ["a"] * action_size + ["a_diff"] * action_size
    print("  " + "".join(["{:11}".format(x) for x in labels]))
    print(result[0])
    print("q MAE: {}".format(mean_abs_q_t))
    print("action MAE: {}\n\n\n".format(mean_abs_action))


def main():
    np.set_printoptions(edgeitems=30, linewidth=100000,
                        formatter=dict(float=lambda x: "{:10.5}".format(x), str=lambda x: "{:10}".format(x)))
    batch1 = np.random.uniform(size=[1, 2, 1]).astype(np.float32)
    batch2 = np.random.uniform(size=[1, 2, 1]).astype(np.float32)
    dataset = [batch1, batch2]

    coder = ActorCritic(4)
    coder_target = ActorCritic(4)
    coder_target.clone_from(coder)

    episodes = EpisodeLog(256)
    mses = []

    adam_critic = tf.train.AdamOptimizer(0.01)
    adam_actor = tf.train.AdamOptimizer(0.001)

    episode_num = 0

    while (True):
        example = random.choice(dataset)
        reward_policy = RewardPolicy()
        env = EnvEncoding(example, reward_policy)
        reward_policy.env = env

        states = []
        actions = []
        rewards = []

        env.coder_reward_listeners.append(lambda x: {rewards.append(x)})

        exploration_episode = random.randint(0, 2) == 0
        # print("EPISODE STARTS ##############")

        episode_length = 0

        coder.reset()

        while not env.code_finished:

            state = env.coder_observe()
            # print("before state")
            # print(env)
            action = coder.actor(state).numpy()
            if exploration_episode:
                # print("RANDOM")
                action += np.concatenate(
                    [np.random.normal(size=[1, 1, 1]), np.random.uniform(0.0, 3.0, size=[1, 1, 3])], 2)
                action = np.concatenate(
                    [np.clip(action[:, :, 0:1], 0, 1), action[:, :, 1:] / np.sum(action[:, :, 1:], 2)], 2)

            # add some noise here

            action_str = env.coder_act(action)
            # print(action_str)
            # print(action)
            #
            # print("after state")
            # print(env)
            # print()
            # print()

            actions.append(action)
            states.append(state)

            episode_length += 1

            if episode_length >= 2 + math.floor(episode_num / 100):
                break

        states.append(env.coder_observe())

        if env.code_finished and episode_length < 5:
            episodes.append((
                np.concatenate(states, 1),
                np.concatenate(actions, 1),
                np.concatenate(rewards, 1)
            ))

        if len(episodes) > 0:
            for _ in range(10):
                train_batch_x, train_batch_y = episodes.prepare_batch(coder_target, 4)
                #
                # print("before training")
                # print_batch(train_batch_x, train_batch_y, coder)
                # print("before training 2")
                # print_batch(train_batch_x, train_batch_y, coder)

                coder.reset()
                with tf.GradientTape() as tape:
                    q = coder.critic(train_batch_x[0], train_batch_x[1])
                    mse = tf.keras.losses.mean_squared_error(train_batch_y, q)
                critic_grads = tape.gradient(mse, coder.critic_variables())
                adam_critic.apply_gradients(zip(critic_grads, coder.critic_variables()))
                mses.append(mse.numpy().mean())

                coder.reset()
                with tf.GradientTape() as tape:
                    minus_q = -coder.critic(train_batch_x[0], coder.actor(train_batch_x[0]))
                actor_grads = tape.gradient(minus_q, coder.actor_variables())
                adam_actor.apply_gradients(zip(actor_grads, coder.actor_variables()))

                # print("after training")
                # print_batch(train_batch_x, train_batch_y, coder)
                # print("\n\n\n\n")

                coder_target.clone_from(coder, 0.0001)

            if episode_num % 100 == 0:
                print("episode_num: {}, dict: {}".format(episode_num, {k: len(v) for k,v in episodes.dict().items()}))
                print_batch(train_batch_x, train_batch_y, coder)


        episode_num += 1

def main2():
    # settings
    np.set_printoptions(edgeitems=30, linewidth=100000,
                        formatter=dict(float=lambda x: "{:10.5}".format(x), str=lambda x: "{:10}".format(x)))
    size = 5
    tick = 0.5


    # drawing
    # x = np.arange(-size, size + tick, tick)
    # y = np.arange(-size, size + tick, tick)
    # qfield = np.zeros([len(x), len(y)])
    # qfield[0, 0] = 1.0
    # u, v = np.meshgrid(x, y)
    fig, ax = plt.subplots()
    ax.set_xlim(-size, size)
    ax.set_ylim(-size, size)
    # quiver = ax.quiver(x, y, u, v)
    # im = ax.imshow(qfield, origin="lower", extent=(-size - .5 * tick, size + .5 * tick, -size - .5 * tick, size + .5 * tick))
    # ax.quiverkey(q, X=0.3, Y=1.1, U=10, label='Quiver key, length = 10', labelpos='E')
    player = ax.scatter([0], [0], c="r")
    text = ax.text(0,0, "init", transform=ax.transAxes)


    def dataset_generate():
        while True:
            yield np.array([[random.uniform(-size, size), random.uniform(-size, size), 0.0, 0.0]]).astype(np.float32)



    dataset = [np.array([[3.0, 3.0, 0.0, 0.0]]).astype(np.float32)]

    model = ActorCritic(2)
    model_target = ActorCritic(2)
    model_target.clone_from(model)

    episodes = EpisodeLog(4096)
    mses = []

    adam_critic = tf.train.AdamOptimizer(0.01)
    adam_actor = tf.train.AdamOptimizer(0.001)

    episode_num = 0
    finished_episodes = 0
    sigma=1e-10

    while (True):
        # example = random.choice(dataset)
        example = next(dataset_generate())
        env = EnvEasy(example)

        states = []
        actions = []
        rewards = []
        not_terminals = []

        exploration_episode = random.random() < 0.9
        player.set_color("r" if exploration_episode else "b")

        # print("EPISODE STARTS ##############")

        episode_length = 0

        model.reset()

        while not env.is_finished():

            state = env.observe()
            not_terminal = env.not_terminal()


            # print("before state")
            # print(env)
            action = model.actor(state).numpy()

            # add some noise here
            if exploration_episode:
                # print("RANDOM")
                sigma = max(10.0-1.0/250.0*episode_num, 0.01)
                action += np.random.normal(0.0, sigma, size=[1, 1, 2])
                norm = np.linalg.norm(action[0,0])
                if norm > 1.0:
                    action /= norm


            reward = env.act(action)

            # print(action_str)
            # print(action)
            #
            # print("after state")
            # print(env)
            # print()
            # print()

            actions.append(action)
            states.append(state)
            rewards.append(reward)
            not_terminals.append(not_terminal)

            episode_length += 1


            # redraw
            if episode_num % 10 ==0:
                player.set_offsets(state[0])
                text.set_text("vel={:6.2},{:6.2}\npos={:6.2},{:6.2}\nep={} step={} break={} fin={}({:6.2})\nsigma={:6.2}".format(
                    env.state[0][2],
                    env.state[0][3],
                    env.state[0][0],
                    env.state[0][1],
                    episode_num,
                    episode_length,
                    episode_num // 100 + 10,
                    finished_episodes,
                    finished_episodes/episode_num if episode_num>0 else "-",
                    sigma
                ))
                # input prep
                # rows = []
                # for py in y:
                #     for px in x:
                #         rows.append([px, py, state.power_up[0, 0], state.power_up[0, 1]])
                # batch = np.array(rows)

                # querying agent
                # actions, q, = agent._session.run([agent._actor_output, agent._critic_output],
                #                                  {agent._actor_input: batch, agent._sigma: agent.calc_sigma()})
                # u, v, qm = actions[:, 0].reshape([-1, len(x)]), actions[:, 1].reshape([-1, len(x)]), q.reshape(-1, len(x))

                # updating
                # quiver.set_UVC(u, v)
                # qm = (qm - np.min(qm)) / (np.max(qm) - np.min(qm))
                # im.set_data(qm)
                plt.pause(0.05)

            if episode_length >= episode_num//100+10:
                break

        states.append(env.observe())
        not_terminals.append(env.not_terminal())

        if env.is_finished():
            finished_episodes += 1

        if episode_length > 0:
            episodes.append((
                np.concatenate(states, 1).astype(np.float32),
                np.concatenate(actions, 1).astype(np.float32),
                np.concatenate(rewards, 1).astype(np.float32),
                np.concatenate(not_terminals, 1).astype(np.float32)
            ))

        if len(episodes) > 0:
            for _ in range(10):
                train_batch_x, train_batch_y, error_mask = episodes.prepare_batch(model_target, 16, error_mask=4.0)
                #
                # print("before training")
                # print_batch(train_batch_x, train_batch_y, coder)
                # print("before training 2")
                # print_batch(train_batch_x, train_batch_y, coder)

                model.reset()
                with tf.GradientTape() as tape:
                    q = model.critic(train_batch_x[0], train_batch_x[1])
                    mse = tf.keras.losses.mean_squared_error(train_batch_y, q)
                    mse_masked = mse * error_mask
                critic_grads = tape.gradient(mse_masked, model.critic_variables())
                adam_critic.apply_gradients(zip(critic_grads, model.critic_variables()))
                mses.append(mse.numpy().mean())

                model.reset()
                with tf.GradientTape() as tape:
                    minus_q = -model.critic(train_batch_x[0], model.actor(train_batch_x[0]))
                actor_grads = tape.gradient(minus_q, model.actor_variables())
                adam_actor.apply_gradients(zip(actor_grads, model.actor_variables()))

                # print("after training")
                # print_batch(train_batch_x, train_batch_y, coder)
                # print("\n\n\n\n")

                model_target.clone_from(model, 0.005)

            if episode_num % 100 == 0:
                print("episode_num: {}, dict: {}".format(episode_num,
                                                         {k: len(v) for k, v in episodes.dict().items()}))
                print_batch(train_batch_x, train_batch_y, model)

        episode_num += 1


def main3():
    e = EnvEasy(np.array([[3.0, 0.0, 0.0, 0.0]]))
    e.act(np.array([[[1.0, 1.0]]]))

if __name__ == "__main__":
    # main()
    main2()
    # main3()
    #
    # c = Coder()
    # c.actor(tf.constant(np.array([[[1.0,0.0]]]).astype(np.float32)))