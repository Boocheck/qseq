from random import choice
from typing import Iterable

from framework.algorithm.stategenerator import StateGenerator
from framework.state import State
from sequence.state import SequenceEnvironmentState


class DummyGenerator(StateGenerator):
    def __init__(self):
        self._states = [
            SequenceEnvironmentState([0.1, 0.2, 0.3, 0.4, 0.5], 0, [0.1, 0.2, 0.3, 0.4, 0.5], 0, [0., 0., 0., 0., 0.], 0),
            SequenceEnvironmentState([0.5, 0.0, 0.5, 0.0], 0, [0.5, 0.0, 0.5, 0.0], 0, [0., 0., 0., 0.], 0),
            # SequenceEnvironmentState([0.1, 0.2, 0.3, 0.4, 0.5], 0, [], 0, [0., 0., 0., 0., 0.], 0),
            # SequenceEnvironmentState([0.5, 0.0, 0.5, 0.0], 0, [], 0, [0., 0., 0., 0.], 0)
        ]

    def generate(self) -> Iterable[SequenceEnvironmentState]:
        while True:
            yield choice(self._states)
