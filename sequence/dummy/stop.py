from framework.algorithm.stop import StopCondition
from framework.environment.state import EnvironmentState
from sequence.state import SequenceEnvironmentState


class CoderDummyStop(StopCondition):
    def is_terminal(self, environment_state: EnvironmentState) -> bool:
        if not isinstance(environment_state, SequenceEnvironmentState):
            raise ValueError()

        return environment_state.image_index >= len(environment_state.image)


class DecoderDummyStop(StopCondition):
    def is_terminal(self, environment_state: EnvironmentState) -> bool:
        if not isinstance(environment_state, SequenceEnvironmentState):
            raise ValueError()

        return environment_state.bits_index >= len(environment_state.bits) and environment_state.decoded_index >= len(environment_state.decoded)
