import numpy as np

from framework.agent.intent import Intent


class CoderIntent(Intent):
    def __init__(self, intent: np.ndarray):
        self.coder_intent = intent
