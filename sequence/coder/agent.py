from typing import List

import numpy as np
import tensorflow as tf

from framework.action import Action
from framework.agent.intent import Intent
from framework.agent.observation import Observation
from framework.agent.state import AgentState
from framework.algorithm.experience import Experience
from framework.algorithm.stop import StopCondition
from framework.environment.state import EnvironmentState
from framework.state import State
from sequence.action import SequenceAction, ActionType
from sequence.agent import SequenceAgent
from sequence.coder.intent import CoderIntent
from sequence.coder.observation import CoderObservation
from sequence.state import SequenceEnvironmentState
from utils.combining import destructuring, combine


class CoderAgent(SequenceAgent):
    def __init__(self, state_size: int, agent_state: "CoderAgentState" = None, stop_condition: StopCondition = None):
        self._state_size = state_size
        self._stop_condition = stop_condition

        self._graph = tf.Graph()
        self._session = tf.Session(graph=self._graph)

        with self._graph.as_default():
            with tf.variable_scope("coder") as coder_scope:
                self._input = tf.placeholder(tf.float32, [None, 2], name="coder_input")
                self._q_target = tf.placeholder(tf.float32, [None, 1], name="coder_q_target")

                with tf.variable_scope("actor") as vs:
                    self._actor_cell = tf.contrib.rnn.LSTMCell(self._state_size)
                    self._actor_state = [tf.placeholder(tf.float32, [None, self._state_size]) for _ in range(2)]
                    self._actor_cell_output, self._actor_new_state = tf.contrib.rnn.static_rnn(self._actor_cell,
                                                                                               [self._input],
                                                                                               initial_state=self._actor_state)
                    self._actor_output = tf.layers.dense(self._actor_cell_output[0], 3, tf.nn.softmax)
                    self._actor_variables = [v for v in tf.global_variables() if
                                             vs.name in v.name and coder_scope.name in v.name]

                with tf.variable_scope("critic") as vs:
                    self._critic_input = tf.concat([self._actor_output, self._input], 1)
                    self._critic_cell = tf.contrib.rnn.LSTMCell(self._state_size)
                    self._critic_state = [tf.placeholder(tf.float32, [None, self._state_size]) for _ in range(2)]
                    self._critic_cell_output, self._critic_new_state = tf.contrib.rnn.static_rnn(self._critic_cell,
                                                                                                 [self._critic_input],
                                                                                                 initial_state=self._critic_state)
                    self._critic_output = tf.layers.dense(self._critic_cell_output[0], 1)
                    self._critic_variables = [v for v in tf.global_variables() if
                                              vs.name in v.name and coder_scope.name in v.name]

                self._actor_minimize = tf.train.AdamOptimizer(0.0001).minimize(tf.reduce_mean(-self._critic_output),
                                                                               var_list=self._actor_variables)
                self._critic_minimize = tf.train.AdamOptimizer(0.0001).minimize(
                    tf.reduce_mean(tf.square(self._critic_output - self._q_target)), var_list=self._critic_variables)

                self.initializer = tf.global_variables_initializer()

        super().__init__(agent_state)
        self._session.run(self.initializer)

    def act(self, intent: Intent) -> Action:
        if not isinstance(intent, CoderIntent):
            raise ValueError()

        action_idx = np.random.choice(3, p=intent.coder_intent.flatten())

        if action_idx in (0, 1):
            action = SequenceAction(ActionType.WRITE_BIT, action_idx)
        elif action_idx == 2:
            action = SequenceAction(ActionType.NEXT_ELEM)
        else:
            raise ValueError()

        return action

    def train(self, experience_batch: List[Experience]):
        dict_before = self._prepare_batch([x.get_before() for x in experience_batch])
        dict_after = self._prepare_batch([x.get_after() for x in experience_batch])
        rewards = [x.get_reward().get_value() for x in experience_batch]

        # q_before = self._session.run(self._critic_output, dict_before)  # TODO[boocheck]: remove this
        q_after = self._session.run(self._critic_output, dict_after)
        q_target = np.array([[rewards[i] + (
            0 if self._stop(experience_batch[i].get_after().get_environment_state()) else 1) * q_after[i, 0]]
                             for i in range(len(experience_batch))])

        dict_before_with_q = dict_before.copy()
        dict_before_with_q.update({
            self._q_target: q_target,
            self._actor_output: combine(experience_batch, lambda x: x.get_intent().coder_intent)
        })

        # print("### before training: ###")
        # print("before|after|target|diff")
        # print(np.concatenate((q_before, q_after, q_target, q_target-q_before), 1))
        # print()

        self._session.run(self._critic_minimize, dict_before_with_q)
        self._session.run(self._actor_minimize, dict_before)

        # TODO[boocheck]: remove this
        # q_before = self._session.run(self._critic_output, dict_before)  # TODO[boocheck]: remove this
        # q_after = self._session.run(self._critic_output, dict_after)
        # q_target = np.array([[rewards[i] + (
        # 1 if self._stop(experience_batch[i].get_after().get_environment_state()) else 0) * q_after[i, 0]]
        #                      for i in range(len(experience_batch))])

        # print("### after training: ###")
        # print("before|after|target|diff")
        # print(np.concatenate((q_before, q_after, q_target, q_target - q_before), 1))
        # print()

    def observe(self, environment_state: EnvironmentState) -> Observation:
        s = environment_state
        if not isinstance(s, SequenceEnvironmentState):
            raise ValueError()

        return CoderObservation(
            np.array([[s.image[s.image_index]]]) if s.image_index < len(s.image) else np.array([[0]]),
            np.array([[0]]) if s.image_index < len(s.image) else np.array([[1]])
        )

    def try_intend(self, observation: Observation) -> (Intent, AgentState):
        if not isinstance(observation, CoderObservation):
            raise ValueError()

        actor_output, actor_state, critic_state = self._session.run(
            (self._actor_output, self._actor_new_state, self._critic_new_state),
            {
                self._input: np.concatenate((observation.image_window, observation.image_finished), 1),
                self._actor_state[0]: self._agent_state.actor_state[0],
                self._actor_state[1]: self._agent_state.actor_state[1],
                self._critic_state[0]: self._agent_state.critic_state[0],
                self._critic_state[1]: self._agent_state.critic_state[1]
            }
        )

        return CoderIntent(actor_output), CoderAgentState(actor_state, critic_state)

    def _create_initial_state(self):
        a, c = [[np.zeros([1, self._state_size]) for x in range(2)] for y in range(2)]
        return CoderAgentState(a, c)

    def _prepare_batch(self, batch: List[State]):
        observations = [self.observe(x.get_environment_state()) for x in batch]

        batch_dict = destructuring({
            self._input: lambda: combine(observations, lambda x: np.concatenate((x.image_window, x.image_finished), 1)),
            self._actor_state[0]: lambda: combine(batch, lambda x: x.get_agent_state().actor_state[0]),
            self._actor_state[1]: lambda: combine(batch, lambda x: x.get_agent_state().actor_state[1]),
            self._critic_state[0]: lambda: combine(batch, lambda x: x.get_agent_state().critic_state[0]),
            self._critic_state[1]: lambda: combine(batch, lambda x: x.get_agent_state().critic_state[1])
        })

        return batch_dict

    def _stop(self, environment_state: EnvironmentState):
        return self._stop_condition.is_terminal(
            environment_state) if self._stop_condition is not None else environment_state.is_terminal()


class CoderAgentState(AgentState):
    def __init__(self, actor_state, critic_state):
        self.actor_state = actor_state
        self.critic_state = critic_state
