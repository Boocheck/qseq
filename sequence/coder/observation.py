import numpy as np

from framework.agent.observation import Observation


class CoderObservation(Observation):
    def __init__(self, image_window: np.ndarray, image_finished: np.ndarray):
        self.image_window = image_window
        self.image_finished = image_finished

    def __repr__(self):
        return "<CoderObservation image_window: {!r} image_finished: {!r}".format(self.image_window, self.image_finished)
