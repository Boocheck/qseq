from enum import Enum

from framework.action import Action


class ActionType(Enum):
    WRITE_BIT = 1
    NEXT_ELEM = 2
    WRITE_ELEM = 3
    NEXT_BIT = 4


class SequenceAction(Action):
    def __init__(self, action_type, data=None):
        self.action_type = action_type
        self.data = data

    def __repr__(self):
        return "<SequenceAction {!r}, {!r}>".format(self.action_type, self.data)



