import numpy as np
import tensorflow as tf

from framework.agent.agent import Agent
from framework.agent.observation import Observation
from framework.environment.state import EnvironmentState

from sequence.state import SequenceEnvironmentState


class SequenceAgent(Agent):
    def sync_to(self, model: Agent, with_state: bool = True, tau: float = 1.0) -> bool:

        if with_state:
            self.set_state(model.get_state())

        variables_number = 0
        variables_errors = 0
        with self._graph.as_default():
            for v in tf.global_variables():
                try:
                    variables_number += 1
                    current = self._session.run(v)
                    other = model._session.run(model._graph.graph.get_tensor_by_name(v.name))
                    new_value = other * tau + (1 - tau) * current
                    v.load(new_value, self._session)
                except Exception as e:
                    variables_errors += 1

        return variables_errors == 0
