from framework.action import Action
from framework.environment.environtment import Environment
from framework.environment.response import EnvironmentResponse
from framework.environment.reward import Reward
from sequence.action import SequenceAction, ActionType
from sequence.state import SequenceEnvironmentState


class SequenceEnvironment(Environment):
    def __init__(self, state):
        super().__init__(state)

    def validate(self, action: Action) -> bool:
        pass

    def react(self, action: Action) -> EnvironmentResponse:

        if isinstance(action, SequenceAction) and isinstance(self._environment_state, SequenceEnvironmentState):
            state = SequenceEnvironmentState(self._environment_state.image.copy(),
                                             self._environment_state.image_index,
                                             self._environment_state.bits.copy(),
                                             self._environment_state.bits_index,
                                             self._environment_state.decoded.copy(),
                                             self._environment_state.decoded_index)
            reward = 0

            if action.action_type == ActionType.NEXT_ELEM:
                if state.image_index < len(state.image):
                    state.image_index += 1
                else:
                    reward = -1
            elif action.action_type == ActionType.WRITE_BIT:
                state.bits.append(int(action.data))
                reward = -1
            elif action.action_type == ActionType.NEXT_BIT:
                if state.bits_index < len(state.bits):
                    state.bits_index += 1
                else:
                    reward = -1
            elif action.action_type == ActionType.WRITE_ELEM:
                if state.decoded_index < len(state.decoded):
                    state.decoded[state.decoded_index] = action.data
                    reward = -3*(abs(state.decoded[state.decoded_index] - state.image[state.decoded_index]))
                    state.decoded_index += 1
                else:
                    reward = -1

            self._environment_state = state
            return EnvironmentResponse(state, Reward(reward))

        else:
            raise ValueError()
