import numpy as np


def combine(collection, selector_func):
    return np.concatenate(list(map(selector_func, collection)), 0)


# def combine_obj(collection):
#     first = collection[0]
#     paths = _crawl(first)
#
#     result = object()
#
#     for p in paths:
#         combined =


def destructuring(obj):
    if callable(obj):
        return obj()

    if isinstance(obj, dict):
        for key in obj:
            obj[key] = destructuring(obj[key])
        return obj

    if isinstance(obj, object):
        for key in obj.__dict__:
            setattr(obj, key, destructuring(getattr(obj, key)))
        return obj

    try:
        iter(obj)
        res = []
        for elem in obj:
            res.append(destructuring(elem))
        return res
    except:
        pass


def _crawl(obj, path=()):
    if isinstance(obj, np.ndarray):
        yield path
    else:
        try:
            iter(obj)
            for i, elem in enumerate(obj):
                for yielded in _crawl(elem, path + (i,)):
                    yield yielded
        except:
            for k in obj.__dict__:
                for elem in _crawl(getattr(obj, k), path + (k,)):
                    yield elem

#
#
#
# def _set(obj, path, val):
#     o = obj
#     p = path[0]
#     n = None if len(path) <= 1 else path[2]
#
#     if not _has(o, p):
#         if n is None:
#             if isinstance(p, str):
#                 o.
#
#
# def _has(o, prop):
#     if isinstance(prop, str):
#         return hasattr(o, prop)
#     else:
#         return len(o) > prop
#
# def _get(obj, path):
#     curr = obj
#
#     for token in path:
#         if isinstance(token, str):
#             curr = getattr(curr, token)
#         elif isinstance(token, int):
#             curr = curr[token]
#         else:
#             raise ValueError()
#
#     return curr
#
# if __name__ == "__main__":
#     state = CoderAgent(10).get_state()
#     for e in _crawl(state):
#         print(e)
#
