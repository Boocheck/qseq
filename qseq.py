import random

import numpy as np
import tensorflow as tf


class Coder:
    def __init__(self, state_size, session):
        with tf.variable_scope("coder") as coder_scope:
            self.session = session
            self.state_size = state_size
            self.input = tf.placeholder(tf.float32, [None, 1])
            self.q_target = tf.placeholder(tf.float32, [None, 1])

            with tf.variable_scope("actor") as vs:
                self.actor_cell = tf.contrib.rnn.LSTMCell(self.state_size)
                self.actor_state = [tf.placeholder(tf.float32, [None, self.state_size]) for _ in range(2)]
                self.actor_cell_output, self.actor_new_state = tf.contrib.rnn.static_rnn(self.actor_cell, [self.input],
                                                                                         initial_state=self.actor_state)
                self.actor_output = tf.layers.dense(self.actor_cell_output[0], 3, tf.nn.softmax)
                self.actor_variables = [v for v in tf.all_variables() if
                                        vs.name in v.name and coder_scope.name in v.name]

            with tf.variable_scope("critic") as vs:
                self.critic_input = tf.concat([self.actor_output, self.input], 1)
                self.critic_cell = tf.contrib.rnn.LSTMCell(self.state_size)
                self.critic_state = [tf.placeholder(tf.float32, [None, self.state_size]) for _ in range(2)]
                self.critic_cell_output, self.critic_new_state = tf.contrib.rnn.static_rnn(self.critic_cell,
                                                                                           [self.critic_input],
                                                                                           initial_state=self.critic_state)
                self.critic_output = tf.layers.dense(self.critic_cell_output[0], 1)
                self.critic_variables = [v for v in tf.all_variables() if
                                         vs.name in v.name and coder_scope.name in v.name]

            self.actor_minimize = tf.train.AdamOptimizer().minimize(tf.reduce_mean(-self.critic_output),
                                                                    var_list=self.actor_variables)
            self.critic_minimize = tf.train.AdamOptimizer().minimize(
                tf.reduce_mean(tf.square(self.critic_output - self.q_target)), var_list=self.critic_variables)

    def eval(self, input, actor_state, critic_state):
        return self.session.run((self.actor_output, self.critic_output, self.actor_new_state, self.critic_new_state),
                                {self.input: input,
                                 self.actor_state[0]: actor_state[0], self.actor_state[1]: actor_state[1],
                                 self.critic_state[0]: critic_state[0], self.critic_state[1]: critic_state[1]})

    def train_actor(self, input, actor_state, critic_state):
        _, q = self.session.run([self.actor_minimize, self.critic_output],
                                {self.input: input,
                                 self.actor_state[0]: actor_state[0], self.actor_state[1]: actor_state[1],
                                 self.critic_state[0]: critic_state[0], self.critic_state[1]: critic_state[1]})
        print("q {:12.10f}".format(np.asscalar(q)))

    def train_critic(self, input, actor_state, critic_state, q_target):
        _, q = self.session.run([self.critic_minimize, self.critic_output],
                                {self.input: input,
                                 self.actor_state[0]: actor_state[0], self.actor_state[1]: actor_state[1],
                                 self.critic_state[0]: critic_state[0], self.critic_state[1]: critic_state[1],
                                 self.q_target: q_target})
        print("q difference {:12.10f} | q {:12.10f}".format(np.asscalar(q) - np.asscalar(q_target), np.asscalar(q)))


class Decoder:
    def __init__(self, state_size, session):
        with tf.variable_scope("decoder"):
            self.session = session
            self.state_size = state_size
            self.input = tf.placeholder(tf.float32, [None, 1])
            self.state = [tf.placeholder(tf.float32, [None, self.state_size]) for _ in range(2)]
            self.cell = tf.contrib.rnn.LSTMCell(self.state_size)
            self.outputs, self.new_state = tf.contrib.rnn.static_rnn(self.cell, [self.input], initial_state=self.state)
            self.action_output = tf.layers.dense(self.outputs[0], 2, tf.nn.softmax)
            self.write_output = tf.layers.dense(self.outputs[0], 1)
            self.q = tf.layers.dense(
                tf.concat([self.input, self.state[0], self.state[1], self.action_output, self.write_output], 1), 1)
            self.q_target = tf.placeholder(tf.float32, [None, 1])
            self.minimize = tf.train.AdamOptimizer().minimize(tf.reduce_mean(tf.square(self.q - self.q_target)))

    def eval(self, input, state):
        return self.session.run((self.action_output, self.write_output, self.new_state, self.q),
                                {self.input: input, self.state[0]: state[0], self.state[1]: state[1]})

    def train(self, input, state, q_target):
        self.session.run(self.minimize,
                         {self.input: input, self.state[0]: state[0], self.state[1]: state[1], self.q_target: q_target})


class CoderRecord:
    def __init__(self, image_window, coder_state, reward):
        self.image_window = image_window
        self.coder_state = coder_state
        self.reward = reward


class DecoderRecord:
    def __init__(self, bitstream_window, decoder_state, reward):
        self.bitstream_window = bitstream_window
        self.decoder_sate = decoder_state
        self.reward = reward


dataset = [[0.1, 0.2, 0.3, 0.4], [0.5, 0.0, 0.5, 0.0, 0.5]]


def get_batch(dataset):
    return random.choice(dataset)


def encode_bit(bit_stream, idx):
    result = (bit_stream[idx] - 0.5) * 2 if idx < len(bit_stream) else 0
    return np.array([[result]])


def main():
    learning_rate = 0.05
    discount_rate = 0.5
    batch_size = 1
    state_size = 10
    sess = tf.Session()
    coder = Coder(state_size, sess)
    decoder = Decoder(state_size, sess)
    sess.run(tf.global_variables_initializer())

    iter = 0
    coder_journal = []
    decoder_journal = []
    while (True):
        iter += 1

        # episode initialize
        example = get_batch(dataset)
        reconstruction = [0] * len(example)
        bit_stream = []
        coder_index = 0
        decoder_reconstruction_index = 0
        decoder_bit_stream_index = 0
        coder_actor_state = [np.zeros([batch_size, state_size]) for _ in range(2)]
        coder_critic_state = [np.zeros([batch_size, state_size]) for _ in range(2)]
        decoder_state = [np.zeros([batch_size, state_size]) for _ in range(2)]

        # coding
        while (coder_index < len(example)):
            coder_input = np.array([[example[coder_index]]])
            action, q, actor_new_state, critic_new_state = coder.eval(coder_input, coder_actor_state,
                                                                      coder_critic_state)
            print(action)

            reward = 0
            action_idx = np.random.choice(3, p=action.flatten())
            if action_idx == 0:
                bit_stream.append(0)
                reward = -1
            elif action_idx == 1:
                bit_stream.append(1)
                reward = -1
            elif action_idx == 2:
                coder_index += 1
            else:
                raise ValueError("wrong action returned from coder")

            # write to journal
            # coder_journal.append((coder_input, coder_actor_state, coder_critic_state, action, reward, np.array([[example[coder_index]]]), actor_new_state, critic_new_state))

            # train coder
            if coder_index < len(example):
                coder_input_future = np.array([[example[coder_index]]])
                _, q_future, _, _ = coder.eval(coder_input_future, actor_new_state, critic_new_state)
                q_target = q + learning_rate * (reward + discount_rate * q_future - q)
            else:
                q_target = np.array([[reward]])

            print(iter)
            print("{} / ({})".format(coder_index, len(example)))
            print(bit_stream)
            coder.train_critic(coder_input, coder_actor_state, coder_critic_state, q_target)
            coder.train_actor(coder_input, coder_actor_state, coder_critic_state)
            print()

            coder_actor_state = actor_new_state
            coder_critic_state = critic_new_state

            # # decoding
            # while (decoder_reconstruction_index < len(example)):
            #
            #     decoder_input = encode_bit(bit_stream, decoder_bit_stream_index)
            #     action, write_output, new_state, q = decoder.eval(decoder_input, decoder_state)
            #
            #     reward = 0
            #     action_idx = np.random.choice(2, p=action.flatten())
            #
            #     if action_idx == 0:
            #         if decoder_bit_stream_index < len(bit_stream) - 1:
            #             decoder_bit_stream_index += 1
            #             reward = 0
            #         else:
            #             reward = -1
            #     elif action_idx == 1:0
            #         if (decoder_reconstruction_index < len(reconstruction)):
            #             reconstruction[decoder_reconstruction_index] += write_output[0][0]
            #             reward = -pow(reconstruction[decoder_reconstruction_index] - example[decoder_reconstruction_index],
            #                           2)
            #             decoder_reconstruction_index += 1
            #         else:
            #             reward = -1
            #     else:
            #         raise ValueError("wrong action returned from decoder")
            #
            #     print(np.array([example, reconstruction]))
            #
            #     # train decoder
            #     decoder_input_targ = encode_bit(bit_stream, decoder_bit_stream_index)
            #     _, _, q_future = coder.eval(decoder_input_targ, new_state)
            #     q_target = q + learning_rate * (reward + discount_rate * q_future - q)
            #     coder.train(decoder_input, decoder_state, q_target)
            #
            #     decoder_state = new_state


main()
